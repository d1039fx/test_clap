import 'package:flutter/material.dart';
import 'package:user_widget_package/user_widget_package.dart';

class Report extends StatelessWidget {
  Report({Key? key}) : super(key: key);

  final Map<String, dynamic> dataUser = {
    'name': 'Daniel Juan',
    'last_name': 'González Hernández',
    'type': 'AUDITORIA',
    'email': 'danieljuan.gonzalez@hemisferiod.co',
    'courses' : {'current': '2', 'total': '5'},
    'synchronous_courses': {'current': '0', 'total': '5'},
    'learning_path': {'current': '0', 'total': '1'},
    'certifications': {'current': '0', 'total': '1'},
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Clap test'),
      ),
      body: UserWidgetPackage(data: dataUser, dataWidget: (value){
        print(value);
      }),
    );
  }
}
