import 'package:flutter/material.dart';
import 'package:test_clap/report/report.dart';

void main() {
  runApp(const TestClap());
}

class TestClap extends StatelessWidget {
  const TestClap({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        useMaterial3: true,
        primarySwatch: Colors.blue,
      ),
      home: Report(),
    );
  }
}
